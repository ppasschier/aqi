<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<title>Upload AQI</title>
<meta name="abstract" content="upload.php">
<meta name="date" content="2020-02-18">
<link rel="icon" href="/favicon.ico">
<link rel="stylesheet" type="text/css" href="/upload.css">
<?php
// The URL of the public viewing page for rerouting:
$url='https://WEB.SITE';
// The HASH that validates the KEY in the /upload.php?key=KEY URL:
$userhash='';
// The KEY that is needed to authenticate to send.php:
$submitkey='';
if(hash('sha256', @$_GET['key'])!=$userhash){
	header('Location: '.$url);
}
?>
<div>
<form action="/send.php" method="POST">
<h2>Complete replacement CSV file:</h2>
<textarea name="file" id="file" rows="6" cols="20" placeholder="Date,Time,AQI
2020-02-02,20:02,202" autofocus></textarea><br>
<input type="hidden" name="key" id="key" value="<?php print($submitkey); ?>">
<button type="submit">Submit</button>
</form>
</div>
