<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<title>Send AQI</title>
<meta name="abstract" content="send.php">
<meta name="date" content="20200218">
<link rel="icon" href="/favicon.ico">
<?php
// The timezone of the location regardless of the location of the server:
$timezone='';
// The URL of the public viewing page for rerouting:
$url='https://WEB.SITE';
// The URL for POSTing the value (same as $url if not set/empty):
$post='';
// The address the mail is to be send to (no mail is send if unset):
$to='';
// The HASH that authenticates the KEY in the form from submit.php:
$submithash='';
// The KEY that is needed to authenticate to index.php:
$sendkey='';
if(hash('sha256', @$_POST['key'])!=$submithash){
	header('Location: '.$url);
	exit(1);
}

function Curlpost($opts){
	global $url, $post;
	$curl=$url;
	if(!empty(@$post)){
		$curl=$post;
	}
	error_log("Curl POST:\n".$opts);
	exec('curl -sd "'.$opts.'" "'.$post.'"');
}

if(!empty($_POST['file'])){ // $file, so check data
	print('<h3>Checking CVS file:</h3><h4>');
	$err=0;
	$nl="\r\n";
	$line=strtok($_POST['file'], $nl);
	if($line!="Date,Time,AQI"){
		print('Proper header missing: <u>'.$line.'</u><br>');
		$err++;
	}
	else {
		print('Header OK: "Date,Time,AQI"<br>');
		$file=$line;
	}
	while($line!==false){
		$line=strtok($nl);
		if(!empty($line) && !preg_match("/^2[0-9][0-9][0-9]-[01][0-9]-[0-3][0-9],[012][0-9]:[0-5][0-9],[1-9][0-9]*$/", $line)){
			print('Syntax not "YYYY-MM-DD,HH:mm,AQI": <u>'.$line.'</u><br>');
			$err++;
		}
		else {
			print('Data OK: "'.$line.'"<br>');
			$file.="\n".$line;
		}
	}
	print('</h4>');
	if($err){
		print('<h3>Go back to try again</h3>');
		exit(3);
	}
	$postfields='key='.$sendkey.'&file='.$file;
	Curlpost($postfields);
	print('<h3>Log file overwritten:</h3>');
	print('<h4>'.str_replace("\n", '<br>', $file).'</h4>');
	exit(0);
}

// Calculate AQI
$p=$_POST['pm25'];
require './ratings.php';
$i=Index($p, 0);
$elm=$ratings[$i];
$maxa=$elm[4];
$maxp=$elm[5];
$mina=$ratings[$i-1][4];
$minp=$ratings[$i-1][5];
$aqi=round(($p-$minp)/($maxp-$minp)*($maxa-$mina)+$mina);

// Curl POST to viewing page
$dt=new DateTime('NOW');
$dt->setTimezone(new DateTimeZone($timezone));
$date=$dt->Format('Y-m-d');
$time=$dt->Format('H:i');
$postfields='date='.$date.'&time='.$time.'&key='.$sendkey.'&aqi='.$aqi;
Curlpost($postfields);

// Mail out if required
if(!empty($to)){
	$subject='[AQI] '.$aqi.' - '.$elm[0];
	$body='The Air Quality Index at LOCATION is '.$aqi.', which means '.
		'a health concern rating of '.$elm[0].' '.$elm[1]."\r\n\r\n".
		'Advisory: '.$elm[2]."\r\n\r\n".
		"The last recorded value can always be viewed at:\r\n".$url.
		"\r\n\r\nRegards,\r\nLOCATION Staff\r\n";
	$from="From: AQI <aqi@WEB.SITE>\r\n";
	$mail=$from.'To: '.$to."\r\nSubject: ".$subject."\r\n\r\n".$body;
	exec('echo "'.$mail.'" |msmtp -X msmtp.log -t');
}

header('Location: '.$url);
?>
