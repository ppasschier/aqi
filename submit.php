<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<title>Submit AQI</title>
<meta name="abstract" content="submit.php">
<meta name="date" content="20200218">
<link rel="icon" href="/favicon.ico">
<link rel="stylesheet" type="text/css" href="/submit.css">
<?php
// The URL of the public viewing page for rerouting:
$url='https://WEB.SITE';
// The HASH that validates the KEY in the /submit.php?key=KEY url:
$userhash='';
// The KEY that is needed to authenticate to send.php:
$submitkey='';
if(hash('sha256', @$_GET['key'])!=$userhash){
	header('Location: '.$url);
}
?>
<div>
<form action="/send.php" method="POST">
<h2>Measured AQI:</h2>
<input type="number" placeholder="AQI" min="0" max="999" name="aqi" id="aqi" autofocus><br>
<input type="hidden" name="key" id="key" value="<?php print($submitkey); ?>">
<button type="submit">Submit</button>
</form>
</div>
