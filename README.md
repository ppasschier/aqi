# aqi

**Display a page with the Air Quality Index that can be updated by
authorized personnel.**

## Deploy
It is possible to only have `index.php` and `index.css` with `ratings.php`,
`logo.png` and `favicon.ico` on a public facing server, and the scripts to
submit data and send emails (`submit.php`, `send.php`, `upload.php` and
`upload.css`) on a local area network (still together with `ratings.php`,
`logo.png` and `favicon.ico`). All files go into the same directory that is
served by a php server (the server built-in to php suffices). The public facing
server needs a subdirectory `log` with full write permissions, to store the log
file (`aqi.csv`).

### Mail
If mail needs to be send, the $to variable in `send.php` needs to be set.
For that to work, the mail function in `send.php` needs to be set up properly!
The line for php's mail() and the code for mtmsp (a small sendmail clone) is
present and an example msmtp configuration file `msmtprc` is included that
needs to live in `/etc`.

Besides $to, the variables $subject, $body and $from in `send.php` could be
customized.

### Variables
Some variables can (or need to be!) set for things to work well:
* $url in `submit.php`/`send.php`: URL where `index.php` is served (important!)
* $timezone in `send.php`: Local timezone (if `send.php` is hosted abroad)
* $to in `send.php`: If filled in, emails are sent there
* $log in `index.php`: Path to log file on the same server as `index.php`
* $userhash in `submit.php`: See Authentication section.
* $submitkey in `submit.php`: See Authentication section.
* $submithash in `send.php`: See Authentication section.
* $sendkey in `send.php`: See Authentication section.
* $sendhash in `index.php`: See Authentication section.

### PHP server security
Access to the log file that the $log variable in `index.php` points to could
be restricted, or its information could stay accessible. If `msmtp` is used as
a mailer, access to the mailer log file `msmtp.log` (possibly on the LAN) also
wants to be restricted. The files `auth` and `README.md` never want (or need)
to be included in any of the served directories!

## Function
* The readers can go to `https://WEB.SITE` to view the current AQI rating.
* The data submitter will go to `https://WEB.SITE/submit.php?key=KEY` to submit an AQI reading.
* The admin could replace the whole log file by going to `https://WEB.SITE/upload.php?key=KEY`

## Authentication
Authentication between the submitter and the scripts and between the scripts
themselves is through KEY / HASH pairs, that ideally should be all different.
These are documented and kept in the file `auth` for reference.

**Authentication is used in 3 places:**
* When requesting to submit a new reading: the KEY is in the URL (`/submit.php?key=KEY`), the HASH is declared in `submit.php`.
* When communicating from `submit.php` to `send.php` (the KEY is in the hidden form input in `send.php` and the HASH is declared in `send.php`).
* When communicating from `send.php` to `index.php` (the KEY is in the curl POST-options in `send.php` and the HASH is declared in `index.php`).

### Renew authentication information
A new KEY / HASH pair can be generated like:
`k=$(uuidgen |tr -d -); h=$(echo -n $k |sha256sum); echo -e "$k\n${h% -}"`
and then be put in their proper places (see above).

