<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<title>AQI</title>
<meta name="abstract" content="index.php">
<meta name="date" content="2020-02-18">
<link rel="icon" href="/favicon.ico">
<link rel="stylesheet" type="text/css" href="/index.css">
<?php
// The HASH that authenticates the KEY in the curl-POST form from send.php:
$sendhash='';
// The location of the log file:
$log='log/aqi.csv';
if(!file_exists($log) || filesize($log)==0){
	if(!file_put_contents($log, "Date,Time,AQI\n")){
		print('<h1>Installation error: file "'.$log.'" is unwritable!</h1>');
		exit(1);
	}
}
require './ratings.php';
if(hash('sha256', @$_POST['key'])==$sendhash){
	if(isset($_POST['file'])){
		$file=$_POST['file'];
		if($file[-1]!="\n"){
			$file.="\n";
		}
		// Replace the whole log file
		error_log('Overwriting log, ');
		error_log(file_put_contents($log, $file, LOCK_EX).' bytes:');
		error_log("\n".$file);
	}
	elseif(!empty($_POST['aqi']) &&	!empty($_POST['date']) &&
			!empty($_POST['time'])){
		// Record data in the log file
		$date=$_POST['date'];
		$time=$_POST['time'];
		$aqi=$_POST['aqi'];
		$line=$date.','.$time.','.$aqi."\n";
		file_put_contents($log, $line, FILE_APPEND | LOCK_EX);
	}
}
else { // Not authenticated
	// Get the last reading from the log file
	$lines=file($log, FILE_IGNORE_NEW_LINES);
	$n_lines=count($lines);
	if($n_lines>1){
		$fields=explode(',', $lines[$n_lines-1]);
		$date=$fields[0];
		$time=$fields[1];
		$aqi=$fields[2];
	}
	else {
		$aqi='AQI';
	}
}
if(!empty($aqi)){
	print('<body style="background-color:'.$ratings[Index($aqi)][3].'"><div>');
	if($aqi!='AQI'){
		print('<h4>on <b>'.$date.'</b> at <b>'.$time.'</b></h4>');
	}
	print('<h1>Air Quality Index</h1><h2>'.$aqi.'</h2>');
	print('<h3>'.$ratings[Index($aqi)][0].'</h3>');
	print('<h5>'.$ratings[Index($aqi)][1].'</h5>');
	print('<h6>Advisory: <b>'.$ratings[Index($aqi)][2].'</b></h6>');
	print('</div>');
}
?>
