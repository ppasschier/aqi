<?php
$ratings=array( // 0:Rating 1:Explanation 2:Advice 3:Colour 4:maxAQI 5:maxPM2.5
	array('Not yet submitted.','','','0',0,0.0),
	array('Green: Good.','Air quality is considered satisfactory, and air pollution poses little or no risk.','None','rgba(0,228,0,.8)',50,12.0),
	array('Yellow: Moderate.','Air quality is acceptable; however, for some pollutants there may be a moderate health concern for a very small number of people who are unusually sensitive to air pollution.','Active children and adults, and people with respiratory disease, such as asthma, should limit prolonged outdoor exertion.','rgba(255,255,0,.8)',100,35.4),
	array('Orange: Unhealthy for Sensitive Groups.','Members of sensitive groups may experience health effects. The general public is not likely to be affected.','Active children and adults, and people with respiratory disease, such as asthma, should limit prolonged outdoor exertion.','rgba(255,126,0,.8)',150,55.4),
	array('Red: Unhealthy.','Everyone may begin to experience health effects; members of sensitive groups may experience more serious health effects.','Active children and adults, and people with respiratory disease, such as asthma, should avoid prolonged outdoor exertion; everyone else, especially children, should limit prolonged outdoor exertion.','rgba(255,0,0,.8)',200,150.4),
	array('Purple: Very Unhealthy.','Health warnings of emergency conditions. The entire population is more likely to be affected.','Active children and adults, and people with respiratory disease, such as asthma, should avoid all outdoor exertion; everyone else, especially children, should limit outdoor exertion.','rgba(153,0,76,.8)',300,250.4),
	array('Maroon: Hazardous.','Health alert: everyone may experience more serious health effects.','Everyone should avoid all outdoor exertion.','rgba(63,0,36,.8)',400,350.4), // no 126 but 63 for contrast
	array('Maroon: Hazardous.','Health alert: everyone may experience more serious health effects.','Everyone should avoid all outdoor exertion.','rgba(63,0,36,.8)',500,500), // no 126 but 63 for contrast
	array('Gray: Unreal','AQI values over 500 are considered beyond the range for AQI.','Stay indoors as much as possible!','rgba(31,31,31,.8)',9999999,9999999),
);
function Index($val, $aqi=1){
// Returns the index for the AQI value (PM2.5 if second parameter is 0)
	global $ratings;
	$i=($aqi ? 4 : 5);
	$j=0;
	foreach($ratings as $elm){
		if(!is_numeric($val) || $val<=$elm[$i]){return $j;}
		++$j;
	}
	return $j;
}
?>
